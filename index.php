<?php get_template_part('templates/page', 'header'); ?>
<div class="container">

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<div class="row">
    <div class="col-md-7">
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>
<?php the_posts_navigation( array(
    'prev_text' => __( '<i class="glyphicon glyphicon-arrow-left"></i>', 'textdomain' ),
    'next_text' => __( '<i class="glyphicon glyphicon-arrow-right"></i>', 'textdomain' ),
) ); ?>
    </div>
    <div class="col-md-4 col-md-offset-1">
    <div class="sidebar">
        <div class="sidebar-search">
            <?php get_search_form(); ?>
        </div>
        
        <div class="tag-list">
        <h3>Tags
        </h3>
        <?php
            if(get_the_tag_list()) {
                echo get_the_tag_list('<ul><li>','</li><li>','</li></ul>');
            }
        ?>  
        </div>

        <div class="archives-list">
        <h3>Archives</h3>
            <ul>
            <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'html', 'show_post_count' => 1 ) ); ?>
            </ul>
        </div>
    </div>
    </div>
    </div>
</div>
