<div class="row">

        <article <?php post_class(); ?>>
        <div class="blog-post">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>

            <div class="tech-image">
                <img src="<?= the_field('technology_image');?>">
            </div>

            <h4><?php the_field('technology_text');?></h4>

            <div class="tech-video">
                <?php the_field('technology_video');?>
            </div>

            <p class="byline author vcard"><?= __('Posted By', 'sage'); ?> <?php get_author_posts_url(get_the_author_meta('ID')); ?><?= get_the_author(); ?></p>
        </div>    
        </article>

</div>
