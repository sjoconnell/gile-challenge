<header>
<div class="header-border">
<div class="container">
  <div class="header-area">

    
      <div class="header-logo">
        <a href="/"><img src="http://gile.dev/wp-content/uploads/2016/02/google-logo.png"></a>
      </div>

      <div class="header-text">
        <h1><a href="#">Official Blog</a></h1>
        <p>Insights from Googlers into our products, technology, and the Google culture</p>
      </div>

  </div> 
</div>
</div>
</header>
