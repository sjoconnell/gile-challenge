<div class="container">
<div class="row">
    <div class="col-md-7">
        <article <?php post_class(); ?>>
        <div class="blog-post">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>

            <div class="tech-image">
                <img src="<?= the_field('technology_image');?>">
            </div>

            <h4><?php the_field('technology_text');?></h4>

            <div class="tech-video">
                <?php the_field('technology_video');?>
            </div>

            <p class="byline author vcard"><?= __('Posted By', 'sage'); ?> <?php get_author_posts_url(get_the_author_meta('ID')); ?><?= get_the_author(); ?></p>
        </div>    
        </article>
    </div>

    <div class="col-md-4 col-md-offset-1">
    <div class="sidebar">
        <div class="sidebar-search">
            <?php get_search_form(); ?>
        </div>
        
        <div class="tag-list">
        <h3>Tags
        </h3>
        <?php
            if(get_the_tag_list()) {
                echo get_the_tag_list('<ul><li>','</li><li>','</li></ul>');
            }
        ?>  
        </div>

        <div class="archives-list">
        <h3>Archives</h3>
            <ul>
            <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'html', 'show_post_count' => 1 ) ); ?>
            </ul>
        </div>
    </div>
    </div>

</div>
</div>