<footer>
    <div class="top-footer">
        <div class="container">
            <div class="footer-box">
                <h2>Company Wide</h2>
                <p><a href="#">Public Policy Blog</a></p>
                <p><a href="#">Research Blog</a></p>
                <p><a href="#">Student Blog</a></p>
            </div>
            <div class="footer-box">
                <h2>Company Wide</h2>
                <p><a href="#">Public Policy Blog</a></p>
                <p><a href="#">Research Blog</a></p>
                <p><a href="#">Student Blog</a></p>
            </div>
            <div class="footer-box">
                <h2>Company Wide</h2>
                <p><a href="#">Public Policy Blog</a></p>
                <p><a href="#">Research Blog</a></p>
                <p><a href="#">Student Blog</a></p>
            </div>
        </div>
    </div>

    <div class="bottom-footer">
        <div class="container">
                <a href="#"><img src="http://gile.dev/wp-content/uploads/2016/02/google-footer-logo.png"></a>

                <ul>
                    <li><a href="#">Google</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Terms</a></li>
                </ul>
        </div>
    </div>    

</footer>
